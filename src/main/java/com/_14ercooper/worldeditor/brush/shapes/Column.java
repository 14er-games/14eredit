package com._14ercooper.worldeditor.brush.shapes;

import com._14ercooper.worldeditor.blockiterator.BlockIterator;
import com._14ercooper.worldeditor.brush.BrushShape;
import com._14ercooper.worldeditor.main.GlobalVars;
import org.bukkit.World;

import java.util.ArrayList;
import java.util.List;

public class Column extends BrushShape {

    @Override
    public BlockIterator GetBlocks(double x, double y, double z, World world) {
        List<String> argList = new ArrayList<>();
        argList.add(Integer.toString((int) x));
        argList.add(Long.toString(GlobalVars.minEditY));
        argList.add(Integer.toString((int) z));
        argList.add(Integer.toString((int) x));
        argList.add(Long.toString(GlobalVars.maxEditY));
        argList.add(Integer.toString((int) z));
        return GlobalVars.iteratorManager.getIterator("cube").newIterator(argList, world);
    }

    @Override
    public void addNewArgument(String argument) {
        // Zero argument brush, do nothing

    }

    @Override
    public boolean lastInputProcessed() {
        return false;
    }

    @Override
    public boolean gotEnoughArgs() {
        return false;
    }

    @Override
    public int minArgCount() {
        return 0;
    }

}
